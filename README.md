# EmailSpider
>爬取coding.net项目广场下所有项目拥有者的Email  
>使用mongodb作为存储数据库  
1. 启动mongodb服务器  
2. 进入项目所在目录，运行命令`npm install`安装项目所需依赖  
3. 运行命令`node spider`,爬虫开始运行  
***  
说明
>可修改setting.js文件修改asyncLink，asyncEmail数值调整并发连接数  
>asyncLink为获取主页链接并发连接数，asyncEmail为获取Email并发连接数.  
>github并发连接数超过20就会出现请求被终止，但是coding.net貌似没有这个问题，并发连接数100也可以正常运行。